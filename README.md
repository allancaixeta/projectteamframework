**ProjectTeamSharedKernel**

This repository contains code to start a fresh VR project for Oculus Quest, which also works for desktops.

It contains the Standard Assets for VR, scripts for reading for inputs for VR, and many scripts for simples VR stuff like buttons.

Authors
Allan Oliveira: https://www.linkedin.com/in/allan-oliveira-22594419/

Leonardo Ademir Tonezi dos Santos: https://www.linkedin.com/in/leonardo-ademir-tonezi-dos-santos-8a431890/

Pedro Henrique Parronchi Lopes: https://www.linkedin.com/in/pedro-henrique-parronchi-165388107/ 



Link do download a package with the framework: https://drive.google.com/file/d/1YZ7LFdiv48SJkSLl6W4VB6WVWQfElEl4/view?usp=sharing
If you download the frawework using the package, you also have to get the InputManager file and put it
in your ProjectSettings folder: https://bitbucket.org/allancaixeta/projectteamframework/src/master/ProjectSettings/InputManager.asset